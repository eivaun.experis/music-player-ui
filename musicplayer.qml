/****************************************************************************
**
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import QtQuick.Window

ApplicationWindow {
    id: window
    width: 1280
    height: 720
    visible: true
    title: "MP3 master 2004"

    property int currentSong: 0
    property var songs: [{
            author: "TIX",
            album: "Single",
            track: "Fallen Angel"
        },{
            author: "Doja Cat",
            album: "Single",
            track: "Kiss Me More (feat. SZA)"
        },{
            author: "Billie Eilish",
            album: "Happier Than Ever",
            track: "Your Power"
        },{
            author: "Halva Priset and Maria Mena",
            album: "Den fineste Chevy'n - Single ",
            track: "Den fineste Chevy'n"
        },{
            author: "Dua lipa",
            album: "Levitating (feat. DaBaby) - Single",
            track: "Levitating (feat. DaBaby)"
        }]

    Component.onCompleted: {
        x = Screen.width / 2 - width / 2
        y = Screen.height / 2 - height / 2
    }

    Shortcut {
        sequence: "Ctrl+Q"
        onActivated: Qt.quit()
    }

    header: ToolBar {
        RowLayout {
            id: headerRowLayout
            anchors.fill: parent
            spacing: 0

            ToolButton {
                icon.name: "grid"
            }
            ToolButton {
                icon.name: "settings"
            }
            ToolButton {
                icon.name: "filter"
            }
            ToolButton {
                icon.name: "message"
            }
            ToolButton {
                icon.name: "music"
            }
            ToolButton {
                icon.name: "cloud"
            }
            ToolButton {
                icon.name: "bluetooth"
            }
            ToolButton {
                icon.name: "cart"
            }
            ToolButton {
                icon.name: "help"
            }

            Item {
                Layout.fillWidth: true
            }

            ToolButton {
                icon.name: "power"
                onClicked: Qt.quit()
            }
        }

        Label {
            text: "MP3 master 2004"
            font.pixelSize: Qt.application.font.pixelSize * 1.3
            anchors.centerIn: headerRowLayout
        }
    }

    footer: ToolBar {
        RowLayout {
            spacing: 10
            anchors.fill: parent
            anchors.leftMargin: 10
            Label { text: "CPU usage: 21%" }
            Label { text: "Memory usage: 827 MB" }
            Item { Layout.fillWidth: true}
        }
    }

    RowLayout {
        spacing: 20
        anchors.fill: parent
        anchors.margins: 20

        ColumnLayout {
            Layout.maximumWidth: 300
            Label {
                text: "Equalizer"
                Layout.alignment: Qt.AlignHCenter
            }
            RowLayout {
                Layout.maximumHeight: 170
                Layout.minimumHeight: 100

                ColumnLayout {
                    Label {
                        text: "12 dB"
                        Layout.fillHeight: true
                    }
                    Label {
                        text: "6 dB"
                        Layout.fillHeight: true
                    }
                    Label {
                        text: "0 dB"
                        Layout.fillHeight: true
                    }
                    Label {
                        text: "-6 dB"
                        Layout.fillHeight: true
                    }
                    Label {
                        text: "-12 dB"
                        Layout.fillHeight: true
                    }
                }

                Repeater {
                    model: 7

                    Slider {
                        value: Math.random()
                        orientation: Qt.Vertical

                        Layout.fillWidth: true
                        Layout.fillHeight: true
                    }
                }
            }
            RowLayout {
                spacing: 10
                ComboBox {
                    currentIndex: 1
                    model: ["Blues", "Classical", "Jazz", "Metal"]
                    Layout.fillWidth: true
                }

                Button {
                    icon.name: "folder"
                }

                Button {
                    icon.name: "save"
                    enabled: false
                }
            }

            Label {
                text: "Volume"
                Layout.alignment: Qt.AlignHCenter
                Layout.topMargin: 20
            }
            Dial {
                Layout.alignment: Qt.AlignHCenter
                Label {
                    text: "0"
                    anchors.bottom: parent.bottom
                    anchors.right: parent.horizontalCenter
                    anchors.rightMargin: parent.height * 0.4
                }
                Label {
                    text: "100"
                    anchors.bottom: parent.bottom
                    anchors.left: parent.horizontalCenter
                    anchors.leftMargin: parent.height * 0.4
                }
            }

            Label {
                text: "Balance"
                Layout.alignment: Qt.AlignHCenter
                Layout.topMargin: 20
            }
            Slider {
                id: balanceSlider
                value: 0
                from: -100
                to: 100

                Layout.fillWidth: true
                ToolTip {
                    parent: balanceSlider.handle
                    visible: balanceSlider.pressed
                    text: value
                    y: parent.height
                    readonly property int value: balanceSlider.valueAt(balanceSlider.position)
                }
                Label {
                    text: "Left"
                }
                Label {
                    anchors.right: parent.right
                    text: "Right"
                }
            }

            Item { Layout.fillHeight: true }

            Label {
                text: "Output device"
            }
            ComboBox {
                Layout.fillWidth: true
                editable: true
                model: ListModel {
                    id: model
                    ListElement {
                        text: "Speakers"
                    }
                    ListElement {
                        text: "Headphones"
                    }
                }
            }
        }

        ColumnLayout {
            Item {
                Layout.fillWidth: true
                Layout.fillHeight: true

                Image {
                    anchors.fill: parent
                    fillMode: Image.PreserveAspectFit
                    source: "images/" + currentSong + ".png"
                }
            }

            Item {
                id: songLabelContainer
                clip: true

                Layout.fillWidth: true
                Layout.preferredHeight: songNameLabel.implicitHeight

                SequentialAnimation {
                    running: true
                    loops: Animation.Infinite

                    PauseAnimation {
                        duration: 2000
                    }
                    ParallelAnimation {
                        XAnimator {
                            target: songNameLabel
                            from: 0
                            to: songLabelContainer.width - songNameLabel.implicitWidth
                            duration: 5000
                        }
                        OpacityAnimator {
                            target: leftGradient
                            from: 0
                            to: 1
                        }
                    }
                    OpacityAnimator {
                        target: rightGradient
                        from: 1
                        to: 0
                    }
                    PauseAnimation {
                        duration: 1000
                    }
                    OpacityAnimator {
                        target: rightGradient
                        from: 0
                        to: 1
                    }
                    ParallelAnimation {
                        XAnimator {
                            target: songNameLabel
                            from: songLabelContainer.width - songNameLabel.implicitWidth
                            to: 0
                            duration: 5000
                        }
                        OpacityAnimator {
                            target: leftGradient
                            from: 0
                            to: 1
                        }
                    }
                    OpacityAnimator {
                        target: leftGradient
                        from: 1
                        to: 0
                    }
                }

                Rectangle {
                    id: leftGradient
                    gradient: Gradient {
                        GradientStop {
                            position: 0
                            color: Material.backgroundColor
                        }
                        GradientStop {
                            position: 1
                            color: Color.transparent(Material.backgroundColor, 0)
                        }
                    }

                    width: height
                    height: parent.height
                    anchors.left: parent.left
                    z: 1
                    rotation: -90
                    opacity: 0
                }

                Label {
                    id: songNameLabel
                    text: songs[currentSong].author + " - " + songs[currentSong].album + " - " + songs[currentSong].track
                    font.pixelSize: Qt.application.font.pixelSize * 1.4
                }

                Rectangle {
                    id: rightGradient
                    gradient: Gradient {
                        GradientStop {
                            position: 0
                            color: Color.transparent(Material.backgroundColor, 0)
                        }
                        GradientStop {
                            position: 1
                            color: Material.backgroundColor
                        }
                    }

                    width: height
                    height: parent.height
                    anchors.right: parent.right
                    rotation: -90
                }
            }

            RowLayout {
                spacing: 8
                Layout.alignment: Qt.AlignHCenter

                RoundButton {
                    icon.name: "favorite"
                    icon.width: 32
                    icon.height: 32
                }
                RoundButton {
                    icon.name: "stop"
                    icon.width: 32
                    icon.height: 32
                }
                RoundButton {
                    icon.name: "previous"
                    icon.width: 32
                    icon.height: 32
                }
                RoundButton {
                    icon.name: "pause"
                    icon.width: 32
                    icon.height: 32
                }
                RoundButton {
                    icon.name: "next"
                    icon.width: 32
                    icon.height: 32
                }
                RoundButton {
                    icon.name: "repeat"
                    icon.width: 32
                    icon.height: 32
                }
                RoundButton {
                    icon.name: "shuffle"
                    icon.width: 32
                    icon.height: 32
                }
            }

            Slider {
                id: seekSlider
                value: 113
                to: 261

                Layout.fillWidth: true

                ToolTip {
                    parent: seekSlider.handle
                    visible: seekSlider.pressed
                    text: pad(Math.floor(value / 60)) + ":" + pad(Math.floor(value % 60))
                    y: parent.height

                    readonly property int value: seekSlider.valueAt(seekSlider.position)

                    function pad(number) {
                        if (number <= 9)
                            return "0" + number;
                        return number;
                    }
                }
            }
        }

        ColumnLayout {
            ButtonGroup {
                buttons: libraryRowLayout.children
            }

            RowLayout {
                id: libraryRowLayout
                Layout.alignment: Qt.AlignHCenter

                Button {
                    text: "Files"
                    checked: true
                    checkable: true
                }
                Button {
                    text: "Playlists"
                    checkable: true
                }
                Button {
                    text: "Favourites"
                    checkable: true
                }
            }

            RowLayout {
                TextField {
                    Layout.fillWidth: true
                    placeholderText: qsTr("Directory path")
                    text: "https://www.top-charts.com/songs/all-genres/norway/total"
                }
                Button {
                    icon.name: "folder"
                }
            }

            Frame {
                id: filesFrame
                leftPadding: 1
                rightPadding: 1

                Layout.fillWidth: true
                Layout.fillHeight: true

                ListView {
                    id: filesListView
                    clip: true
                    anchors.fill: parent

                    model: ListModel {
                        Component.onCompleted: {
                            for (var i = 0; i < songs.length; ++i) {
                                append(songs[i]);
                            }
                        }
                    }
                    delegate: ItemDelegate {
                        text: model.author + " - " + model.album + " - " + model.track
                        width: filesListView.width
                        MouseArea {
                            anchors.fill: parent
                            onClicked: { filesListView.currentIndex = index; }
                        }
                    }
                    highlight: Item {
                        Rectangle {
                            color: Material.accent
                            radius: 5
                            anchors.fill: parent
                            anchors.leftMargin: 5
                            anchors.rightMargin: 5
                        }
                    }
                    ScrollBar.vertical: ScrollBar {
                        parent: filesFrame
                        policy: ScrollBar.AsNeeded
                        anchors.top: parent.top
                        anchors.topMargin: filesFrame.topPadding
                        anchors.right: parent.right
                        anchors.rightMargin: 1
                        anchors.bottom: parent.bottom
                        anchors.bottomMargin: filesFrame.bottomPadding
                    }
                    onCurrentIndexChanged: {
                        currentSong = currentIndex;
                    }

                }
            }
        }

        ColumnLayout {
            AnimatedImage
            {
                id: ad
                Layout.fillWidth: true
                Layout.fillHeight: true
                fillMode: Image.PreserveAspectFit
                source: "images/ad.gif"
                clip: true
            }
        }
    }
}
