TEMPLATE = app
TARGET = musicplayer
QT += quick quickcontrols2

CONFIG += c++17
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000

SOURCES += \
    musicplayer.cpp

RESOURCES += \
    icons/icons.qrc \
    images.qrc \
    imagine-assets/imagine-assets.qrc \
    images.qrc \
    qtquickcontrols2.conf \
    musicplayer.qml \

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
